import { HttpHeaders, HttpParams } from "@angular/common/http";

export const environment = {
  production: false,
  url_api: '/api',
  httpOptions: {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    }),
    params: new HttpParams()
  },
};
