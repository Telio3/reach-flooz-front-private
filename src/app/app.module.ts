import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/pages/home/home.component';
import { AccountComponent } from './components/pages/account/account.component';
import { TakeOffComponent } from './components/pages/take-off/take-off.component';
import { BlackjackComponent } from './components/pages/blackjack/blackjack.component';
import { RankingComponent } from './components/pages/ranking/ranking.component';
import { RouletteComponent } from './components/pages/roulette/roulette.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ROUTE } from './app.routes';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { TokenFormatPipe } from './core/tools/token-format.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AccountComponent,
    TakeOffComponent,
    BlackjackComponent,
    RankingComponent,
    RouletteComponent,
    TokenFormatPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTE),
    FontAwesomeModule,
    BrowserAnimationsModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [CookieService, TokenFormatPipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
