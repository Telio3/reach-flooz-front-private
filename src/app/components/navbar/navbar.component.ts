import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  ViewChild,
} from '@angular/core';

import {
  faSatelliteDish,
  faRocket,
  faMeteor,
  faSatellite,
  faMedal,
  faUserAstronaut,
  faPlay,
  faPause,
  faVolumeUp,
  faVolumeDown,
  faForward,
  IconDefinition,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements AfterViewInit {
  public height: number = window.innerHeight;
  public x: number = 0;
  public y: number = this.height / 2;
  public curveX: number = 10;
  public curveY: number = 0;
  public targetX: number = 0;
  public xitteration: number = 0;
  public yitteration: number = 0;
  public menuExpanded: boolean = false;
  public hoverZone: number = 150;
  public expandAmount: number = 20;

  public faSatelliteDish: IconDefinition = faSatelliteDish;
  public faRocket: IconDefinition = faRocket;
  public faMeteor: IconDefinition = faMeteor;
  public faSatellite: IconDefinition = faSatellite;
  public faMedal: IconDefinition = faMedal;
  public faUserAstronaut: IconDefinition = faUserAstronaut;
  public faStatut: IconDefinition = faPlay;
  public faVolumeUp: IconDefinition = faVolumeUp;
  public faVolumeDown: IconDefinition = faVolumeDown;
  public faForward: IconDefinition = faForward;

  public playlist: string[] = [
    'All_Night.mp3',
    'One_Day.mp3',
    'The_Mojo_Radio_Gang.mp3',
    'YATO.mp3',
    'Coconut.mp3',
    'Sunset.mp3',
    'Ocean.mp3',
  ];
  public indexPlaylist: number = 0;
  public volume: number = 0.05;

  @ViewChild('audioPlayer')
  public audioPlayer!: ElementRef;

  @ViewChild('menu')
  public menu!: ElementRef;

  @ViewChild('blob')
  public blob!: ElementRef;

  @ViewChild('blobPath')
  public blobPath!: ElementRef;

  @ViewChild('hamburger')
  public hamburger!: ElementRef;

  @HostListener('window:mousemove', ['$event'])
  onMouseMove(e: MouseEvent) {
    this.x = e.pageX;
    this.y = e.pageY;
  }

  ngAfterViewInit(): void {
    this.audioPlayer.nativeElement.volume = 0.05;

    window.requestAnimationFrame(() => this.svgCurve());
  }

  playAudio(): void {
    switch (this.faStatut) {
      case faPlay:
        this.faStatut = faPause;
        this.audioPlayer.nativeElement.play();
        break;
      case faPause:
        this.faStatut = faPlay;
        this.audioPlayer.nativeElement.pause();
        break;
    }
  }

  nextAudio(): void {
    this.faStatut = faPause;
    if (this.indexPlaylist == 3) {
      this.indexPlaylist = 0;
    } else {
      ++this.indexPlaylist;
    }
    this.audioPlayer.nativeElement.src =
      '/assets/musics/' + this.playlist[this.indexPlaylist];
    this.audioPlayer.nativeElement.play();
  }

  upVolume(): void {
    if (this.audioPlayer.nativeElement.volume !== 1) {
      this.volume = parseFloat((this.volume += 0.05).toFixed(2));
    }
  }

  downVolume(): void {
    if (this.audioPlayer.nativeElement.volume !== 0) {
      this.volume = parseFloat((this.volume -= 0.05).toFixed(2));
    }
  }

  onMenuExpanded(): void {
    this.menu.nativeElement.classList.add('expanded');
    this.menuExpanded = true;
  }

  outMenuExpanded(): void {
    this.menu.nativeElement.classList.remove('expanded');
    this.menuExpanded = false;
  }

  easeOutExpo(
    currentIteration: number,
    startValue: number,
    changeInValue: number,
    totalIterations: number
  ): number {
    return (
      changeInValue *
        (-Math.pow(2, (-10 * currentIteration) / totalIterations) + 1) +
      startValue
    );
  }

  svgCurve(): void {
    if (this.curveX > this.x - 1 && this.curveX < this.x + 1) {
      this.xitteration = 0;
    } else {
      if (this.menuExpanded) {
        this.targetX = 0;
      } else {
        this.xitteration = 0;
        if (this.x > this.hoverZone) {
          this.targetX = 0;
        } else {
          this.targetX = -(
            ((60 + this.expandAmount) / 100) *
            (this.x - this.hoverZone)
          );
        }
      }
      this.xitteration++;
    }

    if (this.curveY > this.y - 1 && this.curveY < this.y + 1) {
      this.yitteration = 0;
    } else {
      this.yitteration = 0;
      this.yitteration++;
    }

    this.curveX = this.easeOutExpo(
      this.xitteration,
      this.curveX,
      this.targetX - this.curveX,
      100
    );
    this.curveY = this.easeOutExpo(
      this.yitteration,
      this.curveY,
      this.y - this.curveY,
      100
    );

    let anchorDistance = 200;
    let curviness = anchorDistance - 40;

    let newCurve2 =
      'M60,' +
      this.height +
      'H0V0h60v' +
      (this.curveY - anchorDistance) +
      'c0,' +
      curviness +
      ',' +
      this.curveX +
      ',' +
      curviness +
      ',' +
      this.curveX +
      ',' +
      anchorDistance +
      'S60,' +
      this.curveY +
      ',60,' +
      (this.curveY + anchorDistance * 2) +
      'V' +
      this.height +
      'z';

    this.blobPath.nativeElement.setAttribute('d', newCurve2);

    this.blob.nativeElement.style.width = this.curveX + 60;

    this.hamburger.nativeElement.style.transform =
      'translate(' + this.curveX + 'px, ' + this.curveY + 'px)';

    window.requestAnimationFrame(() => this.svgCurve());
  }
}
