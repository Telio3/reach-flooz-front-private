import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Card } from 'src/app/core/models/card.model';
import * as confetti from 'canvas-confetti';

import { faSort, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/core/services/auth.service';
import { UserApiService } from 'src/app/core/services/api/user-api.service';
import { User } from 'src/app/core/models/user.model';

@Component({
  selector: 'app-blackjack',
  templateUrl: './blackjack.component.html',
  styleUrls: ['./blackjack.component.scss'],
})
export class BlackjackComponent implements OnInit {
  public isLoading: boolean = true;
  public user: User | null = null;
  
  public faSort: IconDefinition = faSort;

  public suits: string[] = ['Hearts', 'Clubs', 'Diamonds', 'Spades'];
  public numbers: number[] = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10];
  public index: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
  public values: string[] = [
    'Ace',
    'Two',
    'Three',
    'Four',
    'Five',
    'Six',
    'Seven',
    'Eight',
    'Nine',
    'Ten',
    'Jack',
    'Queen',
    'King',
  ];
  public deck: Card[] = [];

  @ViewChild('nbBet')
  public nbBetElement!: ElementRef;
  public nbChips: number = 300000;
  public blockInput: boolean = false;
  public displayAddChips: boolean = false;
  public addChips: number = 0;

  @ViewChild('returnedCardContainer')
  public returnedCardContainer!: ElementRef;
  @ViewChild('returnedCard')
  public returnedCardElement!: ElementRef;
  public returnedCard!: Card;

  public nbDealer: number = 0;
  public nbDealerElement: string = '0';
  public cardsDealer: Card[] = [];
  public aceDealer: boolean = false;
  public doAceDealer: boolean = false;
  public firstThreeCards: Card[] = [];

  public nbPlayer: number = 0;
  public nbPlayerElement: string = '0';
  public cardsPlayer: Card[] = [];
  public acePlayer: boolean = false;
  public doAcePlayer: boolean = false;

  public choiceHand: number = 0;

  public displayContainerBet: boolean = false;

  @ViewChild('pairsJeton')
  public pairsJetonElement!: ElementRef;
  public displayPairsJeton: boolean = false;
  public betPairs: number = 0;
  public restartBetPairs: number = 0;
  public winPairs: boolean = false;

  @ViewChild('mainJeton')
  public mainJetonElement!: ElementRef;
  public displayMainJeton: boolean = false;
  public betMain: number = 0;
  public restartBetMain: number = 0;

  @ViewChild('comboJeton')
  public comboJetonElement!: ElementRef;
  public displayComboJeton: boolean = false;
  public betCombo: number = 0;
  public restartBetCombo: number = 0;
  public winCombo: boolean = false;

  public winConfetti: any;
  public displayHitButton: boolean = false;
  public displayBetButton: boolean = true;
  public activeBetButton: boolean = false;
  public displayStayButton: boolean = false;
  public displayRestartButton: boolean = false;
  public displayDoubleButton: boolean = false;
  public displayDivideButton: boolean = false;
  public endGameStatut: boolean = false;

  constructor(private authService: AuthService, private userApi: UserApiService) {}

  ngOnInit(): void {
    this.isLoading = false;
    
    this.authService.getUserById()?.subscribe(
      (data) => {
        if (data) {
          this.user = data;
          this.nbChips = this.user.chips;
          this.isLoading = true;
        } else {
          this.user = null;
          this.isLoading = true;
        }
      },
      (err) => {
        this.user = null;
        this.isLoading = true;
      }
    );

    let fireworksCanvas = document.createElement('canvas');
    fireworksCanvas.width = window.innerWidth;
    fireworksCanvas.height = window.innerHeight;
    fireworksCanvas.style.position = 'fixed';
    fireworksCanvas.style.pointerEvents = 'none';

    document.body.appendChild(fireworksCanvas);

    this.winConfetti = confetti.create(fireworksCanvas, {});
  }

  playAudio(audioPath: string): void {
    const audio: HTMLAudioElement = new Audio(audioPath);
    audio.volume = 0.25;
    audio.play();
  }

  fire(particleRatio: number, opts: Object): void {
    let count: number = 200;
    let defaults: Object = {
      origin: { y: 0.7 },
    };

    this.winConfetti(
      Object.assign({}, defaults, opts, {
        particleCount: Math.floor(count * particleRatio),
      })
    );
  }

  fireworks(): void {
    this.fire(0.25, {
      spread: 26,
      startVelocity: 55,
    });
    this.fire(0.2, {
      spread: 60,
    });
    this.fire(0.35, {
      spread: 100,
      decay: 0.91,
      scalar: 0.8,
    });
    this.fire(0.1, {
      spread: 120,
      startVelocity: 25,
      decay: 0.92,
      scalar: 1.2,
    });
    this.fire(0.1, {
      spread: 120,
      startVelocity: 45,
    });
  }

  nextNum(): void {
    let currentBet: number = 0;
    switch (this.choiceHand) {
      case 1:
        currentBet = this.betPairs;
        break;
      case 2:
        currentBet = this.betMain;
        break;
      case 3:
        currentBet = this.betCombo;
        break;
    }

    if (!this.blockInput) {
      let bet: number = parseInt(this.nbBetElement.nativeElement.value);

      if (bet < this.nbChips - 99 + currentBet) {
        bet = bet + 100;
        this.nbBetElement.nativeElement.value = bet.toString();
      }
    }
  }

  prevNum(): void {
    if (!this.blockInput) {
      let bet: number = parseInt(this.nbBetElement.nativeElement.value);

      if (bet >= 100) {
        bet = bet - 100;
        this.nbBetElement.nativeElement.value = bet.toString();
      }
    }
  }

  maxBet(): void {
    let currentBet: number = 0;
    switch (this.choiceHand) {
      case 1:
        currentBet = this.betPairs;
        break;
      case 2:
        currentBet = this.betMain;
        break;
      case 3:
        currentBet = this.betCombo;
        break;
    }

    let bet: number = parseInt(this.nbBetElement.nativeElement.value);

    if (bet > this.nbChips + currentBet) {
      this.nbBetElement.nativeElement.value = '0';
    } else if (bet < 0) {
      this.nbBetElement.nativeElement.value = '0';
    } else {
      this.nbBetElement.nativeElement.value = bet.toString();
    }
  }

  restart(): void {
    if (this.restartBetPairs) {
      this.choiceHand = 1;
      this.nbBetElement.nativeElement.value = this.restartBetPairs;
      this.checkBet();
    }

    if (this.restartBetMain) {
      this.choiceHand = 2;
      this.nbBetElement.nativeElement.value = this.restartBetMain;
      this.checkBet();
    }

    if (this.restartBetCombo) {
      this.choiceHand = 3;
      this.nbBetElement.nativeElement.value = this.restartBetCombo;
      this.checkBet();
    }

    this.displayRestartButton = false;
  }

  double(): void {
    this.choiceHand = 2;
    this.nbChips -= this.betMain;
    this.betMain += this.betMain;

    if (this.user) {
      this.userApi.updateUserChips({ chips: this.nbChips }, this.user._id).subscribe();
    }

    this.hit(true);

    setTimeout(() => {
      if (this.nbPlayer < 21) {
        this.stay();
      }
    }, 1000);
  }

  divide(): void {}

  createDeck(): void {
    for (let nbDeck = 1; nbDeck <= 6; nbDeck++) {
      for (let suitIdx = 0; suitIdx < this.suits.length; suitIdx++) {
        for (let valueIdx = 0; valueIdx < this.values.length; valueIdx++) {
          let color: string = '';

          if (this.suits[suitIdx] === 'Clubs' || this.suits[suitIdx] === 'Spades') {
            color = 'black';
          } else {
            color = 'red';
          }

          let card = {
            number: this.numbers[valueIdx],
            value: this.values[valueIdx],
            path: '/assets/images/cartes/' + this.suits[suitIdx] + '-' + this.values[valueIdx] + '.png',
            suit: this.suits[suitIdx],
            color: color,
            index: this.index[valueIdx],
            isReturned: false,
          };
          this.deck.push(card);
        }
      }
    }

    this.shuffleArray(this.deck);
    this.shuffleArray(this.deck);
  }

  shuffleArray(array: Card[]): void {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }

  giveCard(typeCard: string): Card {
    switch (typeCard) {
      case 'cardDealer':
        this.cardsDealer.push(this.deck[0]);

        if (this.deck[0].number != 11) {
          this.nbDealer += this.deck[0].number;

          if (this.aceDealer) {
            if (this.nbDealer > 21) {
              if (!this.doAceDealer) {
                this.nbDealer -= 10;
                this.doAceDealer = true;
              }
            }
          }
        } else {
          if (this.aceDealer) {
            this.nbDealer += 1;
          } else {
            if (this.nbDealer < 11) {
              this.nbDealer += 11;
              this.aceDealer = true;
            } else {
              this.nbDealer += 1;
            }
          }
        }
        if (this.aceDealer) {
          if (this.nbDealer < 21) {
            if (!this.doAceDealer) {
              this.nbDealerElement = (this.nbDealer - 10).toString() + ' / ' + this.nbDealer.toString();
            } else {
              this.nbDealerElement = this.nbDealer.toString();
            }
          } else {
            this.nbDealerElement = this.nbDealer.toString();
          }
        } else {
          this.nbDealerElement = this.nbDealer.toString();
        }
        break;
      case 'cardPlayer':
        this.cardsPlayer.push(this.deck[0]);
        if (this.deck[0].number != 11) {
          this.nbPlayer += this.deck[0].number;

          if (this.acePlayer) {
            if (this.nbPlayer > 21) {
              if (!this.doAcePlayer) {
                this.nbPlayer -= 10;
                this.doAcePlayer = true;
              }
            }
          }
        } else {
          if (this.acePlayer) {
            this.nbPlayer += 1;
          } else {
            if (this.nbPlayer < 11) {
              this.nbPlayer += 11;
              this.acePlayer = true;
            } else {
              this.nbPlayer += 1;
            }
          }
        }
        if (this.acePlayer) {
          if (this.nbPlayer < 21) {
            if (!this.doAcePlayer) {
              this.nbPlayerElement = (this.nbPlayer - 10).toString() + ' / ' + this.nbPlayer.toString();
            } else {
              this.nbPlayerElement = this.nbPlayer.toString();
            }
          } else {
            this.nbPlayerElement = this.nbPlayer.toString();
          }
        } else {
          this.nbPlayerElement = this.nbPlayer.toString();
        }
        break;
    }

    let card = this.deck[0];
    this.deck.shift();

    this.playAudio('/assets/effects/give-card.mp3');

    return card;
  }

  startGame(): void {
    if (this.user) {
      this.userApi.updateUserChips({ chips: this.nbChips }, this.user._id).subscribe();
    }
    
    this.firstThreeCards.push(this.giveCard('cardPlayer'));

    setTimeout(() => {
      this.firstThreeCards.push(this.giveCard('cardDealer'));
    }, 700);

    setTimeout(() => {
      this.firstThreeCards.push(this.giveCard('cardPlayer'));
      this.checkSideBet();
    }, 1400);

    setTimeout(() => {
      this.returnedCard = this.deck[0];
      this.returnedCard.isReturned = true;
      this.cardsDealer.push(this.returnedCard);
      this.deck.shift();

      this.playAudio('/assets/effects/give-card.mp3');

      if (this.nbPlayer == 21) {
        setTimeout(() => {
          this.returnedCardElement.nativeElement.src = this.returnedCard.path;
          this.returnedCardContainer.nativeElement.classList.add('is-returned');

          this.playAudio('/assets/effects/card-flip.mp3');

          if (this.returnedCard.number == 11) {
            this.nbDealer += 11;
          } else {
            this.nbDealer += this.returnedCard.number;
          }

          this.nbDealerElement = this.nbDealer.toString();

          if (this.nbDealer != 21) {
            this.betMain *= 1.25;
            this.winAnimation();
          } else {
            this.equalityAnimation();
          }
        }, 500);
      } else {
        setTimeout(() => {
          if (this.betMain <= this.nbChips) {
            this.displayDoubleButton = true;
          }

          this.displayHitButton = true;
          this.displayStayButton = true;
        }, 1000);
      }
    }, 2100);
  }

  bet(): void {
    if (this.deck.length < 100) {
      this.createDeck();
    } else {
      this.nbPlayerElement = '0';
      this.nbDealerElement = '0';
    }

    this.cardsDealer = [];
    this.cardsPlayer = [];
    this.firstThreeCards = [];
    this.returnedCard = {
      number: 0,
      value: '',
      path: '',
      suit: '',
      color: '',
      index: 0,
      isReturned: true,
    };
    this.nbDealer = 0;
    this.nbPlayer = 0;
    this.aceDealer = false;
    this.acePlayer = false;
    this.doAceDealer = false;
    this.doAcePlayer = false;
    this.displayBetButton = false;
    this.activeBetButton = false;
    this.displayRestartButton = false;
    this.startGame();
  }

  hit(isDouble?: boolean): void {
    this.displayDoubleButton = false;
    this.displayHitButton = false;
    this.displayStayButton = false;

    setTimeout(() => {
      this.giveCard('cardPlayer');

      if (this.nbPlayer > 21) {
        setTimeout(() => {
          this.returnedCardElement.nativeElement.src = this.returnedCard.path;
          this.returnedCardContainer.nativeElement.classList.add('is-returned');

          this.playAudio('/assets/effects/card-flip.mp3');

          this.nbDealerElement = this.nbDealer.toString();
        }, 400);
        if (this.returnedCard.number == 11) {
          if (this.aceDealer) {
            this.nbDealer += 1;
          } else {
            if (this.nbDealer < 11) {
              this.nbDealer += 11;
              this.aceDealer = true;
            } else {
              this.nbDealer += 1;
            }
          }
        } else {
          this.nbDealer += this.returnedCard.number;
        }

        this.loseAnimation();
      } else if (this.nbPlayer == 21) {
        this.stay();
      } else {
        setTimeout(() => {
          if (!isDouble) {
            this.displayHitButton = true;
            this.displayStayButton = true;
          }
        }, 500);
      }
    }, 700);
  }

  stay(): void {
    this.displayDoubleButton = false;
    this.displayHitButton = false;
    this.displayStayButton = false;

    setTimeout(() => {
      this.returnedCardElement.nativeElement.src = this.returnedCard.path;
      this.returnedCardContainer.nativeElement.classList.add('is-returned');

      this.playAudio('/assets/effects/card-flip.mp3');

      if (this.returnedCard.number == 11) {
        if (this.aceDealer) {
          this.nbDealer += 1;
        } else {
          if (this.nbDealer < 11) {
            this.nbDealer += 11;
            this.aceDealer = true;
          } else {
            this.nbDealer += 1;
          }
        }
      } else {
        this.nbDealer += this.returnedCard.number;
      }

      this.nbDealerElement = this.nbDealer.toString();
      this.nbPlayerElement = this.nbPlayer.toString();
      this.endGame();
    }, 700);
  }

  endGame(): void {
    this.nbDealerElement = this.nbDealer.toString();

    if (this.nbDealer < 17) {
      setTimeout(() => {
        this.giveCard('cardDealer');
        this.endGame();
      }, 700);
    } else if (this.nbPlayer > 21) {
      this.loseAnimation();
    } else if (this.nbDealer >= 17 && this.nbDealer <= 21) {
      if (this.nbDealer > this.nbPlayer) {
        this.loseAnimation();
      } else if (this.nbDealer < this.nbPlayer) {
        this.winAnimation();
      } else if (this.nbDealer == this.nbPlayer) {
        this.equalityAnimation();
      }
    } else if (this.nbDealer > 21 && this.nbPlayer <= 21) {
      this.winAnimation();
    }
  }

  setHand(jeton: number): void {
    this.displayContainerBet = true;
    this.choiceHand = jeton;
  }

  checkSideBet(): void {
    if (this.betPairs) {
      let multiplierBetPairs: number = 0;

      if (this.firstThreeCards[0].value === this.firstThreeCards[2].value) {
        if (this.firstThreeCards[0].color === this.firstThreeCards[2].color) {
          if (this.firstThreeCards[0].suit === this.firstThreeCards[2].suit) {
            multiplierBetPairs = 25;
          } else {
            multiplierBetPairs = 12;
          }
        } else {
          multiplierBetPairs = 6;
        }
      }

      this.betPairs *= multiplierBetPairs;

      if (!this.betPairs) {
        this.pairsJetonElement.nativeElement.classList.add('exit-jeton');
        this.playAudio('/assets/effects/chip.mp3');

        setTimeout(() => {
          this.displayPairsJeton = false;
        }, 399);
      } else {
        this.winPairs = true;
      }
    }

    if (this.betCombo) {
      let multiplierBetCombo: number = 0;

      const isCardSuite = (): boolean => {
        let arrayCardValue: number[] = [];

        arrayCardValue.push(this.firstThreeCards[0].index);
        arrayCardValue.push(this.firstThreeCards[1].index);
        arrayCardValue.push(this.firstThreeCards[2].index);

        let uniqueValue: number[] = [...new Set(arrayCardValue)];

        if (uniqueValue.length === 3) {
          const compare = (value1: number, value2: number) => {
            return value1 - value2;
          };

          arrayCardValue.sort(compare);

          const differenceAry: number[] = arrayCardValue
            .slice(1)
            .map((n, i) => {
              return n - arrayCardValue[i];
            });
          const isDifference: boolean = differenceAry.every(
            (value) => value == 1
          );

          return isDifference;
        }

        return false;
      };

      if (this.firstThreeCards[0].color === this.firstThreeCards[1].color && this.firstThreeCards[1].color === this.firstThreeCards[2].color) {
        multiplierBetCombo = 5;
      }

      if (isCardSuite()) {
        multiplierBetCombo = 10;

        if (this.firstThreeCards[0].color === this.firstThreeCards[1].color && this.firstThreeCards[1].color === this.firstThreeCards[2].color) {
          multiplierBetCombo = 40;
        }
      }

      if (this.firstThreeCards[0].value === this.firstThreeCards[1].value && this.firstThreeCards[1].value === this.firstThreeCards[2].value) {
        multiplierBetCombo = 30;

        if (this.firstThreeCards[0].suit === this.firstThreeCards[1].suit && this.firstThreeCards[1].suit === this.firstThreeCards[2].suit) {
          multiplierBetCombo = 100;
        }
      }

      this.betCombo *= multiplierBetCombo;

      if (!this.betCombo) {
        this.comboJetonElement.nativeElement.classList.add('exit-jeton');
        this.playAudio('/assets/effects/chip.mp3');

        setTimeout(() => {
          this.displayComboJeton = false;
        }, 399);
      } else {
        this.winCombo = true;
      }
    }
  }

  resetBet(showBetButton: boolean): void {
    setTimeout(() => {
      this.endGameStatut = true;

      document
        .querySelectorAll('.card-blackjack, .switch-card')
        .forEach((card) => {
          const cardElement = card as HTMLDivElement;
          cardElement.style.top = '10%';
          cardElement.style.left = '2%';
          cardElement.style.marginLeft = '0';
          cardElement.style.transform = 'rotate(1deg)';
          cardElement.style.transition = 'all .4s;';

          setTimeout(() => {
            cardElement.style.top = '-100%';
          }, 500);
        });

      if (this.displayPairsJeton) {
        this.pairsJetonElement.nativeElement.classList.add('exit-jeton');
      }

      this.mainJetonElement.nativeElement.classList.add('exit-jeton');

      if (this.displayComboJeton) {
        this.comboJetonElement.nativeElement.classList.add('exit-jeton');
      }

      setTimeout(() => {
        this.displayPairsJeton = false;
        this.betPairs = 0;
        this.winPairs = false;

        this.displayMainJeton = false;
        this.betMain = 0;

        this.displayComboJeton = false;
        this.betCombo = 0;
        this.winCombo = false;

        if (this.restartBetPairs + this.restartBetMain + this.restartBetCombo <= this.nbChips) {
          this.displayRestartButton = true;
        }

        if (showBetButton) {
          this.displayBetButton = true;
        }
      }, 399);
    }, 500);

    setTimeout(() => {
      this.displayAddChips = false;
    }, 3000);
  }

  winAnimation(): void {
    let addChips: number = 0;

    addChips = this.betMain * 2;

    if (this.winPairs) {
      addChips += this.betPairs;
      this.winPairs = false;
    }

    if (this.winCombo) {
      addChips += this.betCombo;
      this.winCombo = false;
    }

    this.addChips = addChips;
    this.displayAddChips = true;

    this.nbChips += addChips;

    if (this.user) {
      this.userApi.updateUserChips({ chips: this.nbChips }, this.user._id).subscribe();
    }

    console.log('Gagné !');

    setTimeout(() => {
      this.fireworks();
      this.playAudio('/assets/effects/confetti.mp3');

      setTimeout(() => {
        this.displayBetButton = true;
      }, 4000);
    }, 500);

    this.resetBet(false);
  }

  loseAnimation(): void {
    this.resetBet(true);
    console.log('Perdu !');
  }

  equalityAnimation(): void {
    this.addChips = this.betMain;
    this.displayAddChips = true;

    this.nbChips += this.betMain;

    if (this.user) {
      this.userApi.updateUserChips({ chips: this.nbChips }, this.user._id).subscribe();
    }

    console.log('Egalité !');

    this.resetBet(true);
  }

  checkBet(): void {
    let bet = parseInt(this.nbBetElement.nativeElement.value);

    switch (this.choiceHand) {
      case 1:
        if (bet != 0) {
          this.displayPairsJeton = true;
          this.nbChips += this.betPairs;
          this.nbChips -= bet;
        } else if (this.displayPairsJeton) {
          this.pairsJetonElement.nativeElement.classList.add('exit-jeton');
          this.nbChips += this.betPairs;
          setTimeout(() => {
            this.displayPairsJeton = false;
          }, 399);
        }

        this.betPairs = bet;
        this.restartBetPairs = this.betPairs;
        break;
      case 2:
        if (bet != 0) {
          this.displayMainJeton = true;
          this.activeBetButton = true;
          this.nbChips += this.betMain;
          this.nbChips -= bet;
        } else if (this.displayMainJeton) {
          this.activeBetButton = false;
          this.mainJetonElement.nativeElement.classList.add('exit-jeton');
          this.nbChips += this.betMain;
          setTimeout(() => {
            this.displayMainJeton = false;
          }, 399);
        }

        this.betMain = bet;
        this.restartBetMain = this.betMain;
        break;
      case 3:
        if (bet != 0) {
          this.displayComboJeton = true;
          this.nbChips += this.betCombo;
          this.nbChips -= bet;
        } else if (this.displayComboJeton) {
          this.comboJetonElement.nativeElement.classList.add('exit-jeton');
          this.nbChips += this.betCombo;
          setTimeout(() => {
            this.displayComboJeton = false;
          }, 399);
        }

        this.betCombo = bet;
        this.restartBetCombo = this.betCombo;
        break;
    }

    this.displayRestartButton = false;
    this.displayContainerBet = false;
    this.nbBetElement.nativeElement.value = 0;
  }
}

// Animation Lose / Win / Equality
// Divide button
