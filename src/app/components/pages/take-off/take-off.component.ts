import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/core/models/user.model';
import { UserApiService } from 'src/app/core/services/api/user-api.service';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-take-off',
  templateUrl: './take-off.component.html',
  styleUrls: ['./take-off.component.scss'],
})
export class TakeOffComponent implements OnInit {
  public isLoading: boolean = true;
  public user: User | null = null;
  public nbChips: number = 300;
  public blockInput: boolean = false;
  public blockAllInButton: boolean = false;
  public counter: number = 1.0;
  public maxCounter: number = 0.0;
  public duration: number = 60;
  public historicalList: number[] = [];
  public displaySpaceStation: boolean = true;
  public displayRocket: boolean = true;
  public displayCounter: boolean = false;
  public displayAstronaut: boolean = true;
  public displayExplosion: boolean = true;
  public displayAddChips: boolean = false;
  public addChips: number = 0;
  public textButtonAction: string = 'Miser';
  public visibilityButtonAction: boolean = true;
  public buttonAction: string = 'bet';

  @ViewChild('nbBet')
  public nbBetElement!: ElementRef;

  @ViewChild('historical')
  public historicalElement!: ElementRef;

  constructor(
    private authService: AuthService,
    private userApi: UserApiService
  ) {}

  ngOnInit(): void {
    this.isLoading = false;
    
    this.authService.getUserById()?.subscribe(
      (data) => {
        if (data) {
          this.user = data;
          this.nbChips = this.user.chips;
          this.isLoading = true;
        } else {
          this.user = null;
          this.isLoading = true;
        }
      },
      (err) => {
        this.user = null;
        this.isLoading = true;
      }
    );

    this.displayRocket = false;
    this.displayAstronaut = false;
    this.displayExplosion = false;
  }

  nextNum(): void {
    if (!this.blockInput) {
      let bet: number = parseInt(this.nbBetElement.nativeElement.value);

      if (bet < this.nbChips - 99) {
        bet = bet + 100;
        this.nbBetElement.nativeElement.value = bet.toString();
      }
    }
  }

  prevNum(): void {
    if (!this.blockInput) {
      let bet: number = parseInt(this.nbBetElement.nativeElement.value);

      if (bet >= 100) {
        bet = bet - 100;
        this.nbBetElement.nativeElement.value = bet.toString();
      }
    }
  }

  maxBet(): void {
    let bet: number = parseInt(this.nbBetElement.nativeElement.value);
    if (bet > this.nbChips) {
      this.nbBetElement.nativeElement.value = '0';
    } else if (bet < 0) {
      this.nbBetElement.nativeElement.value = '0';
    } else {
      this.nbBetElement.nativeElement.value = bet.toString();
    }
  }

  allIn(): void {
    this.nbBetElement.nativeElement.value = this.nbChips;
  }

  randomInt(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  randomFloat(min: number, max: number): number {
    return parseFloat((Math.random() * (min - max) + max).toFixed(2));
  }

  action(): void {
    switch (this.buttonAction) {
      case 'bet':
        this.bet();
        break;
      case 'withdraw':
        this.withdraw();
        break;
      case 'skip':
        this.skip();
        break;
      default:
        break;
    }
  }

  bet(): void {
    if (this.nbBetElement.nativeElement.value != 0) {
      this.blockInput = true;
      this.blockAllInButton = true;

      this.textButtonAction = 'Retirer';
      this.buttonAction = '';
      setTimeout(() => {
        this.buttonAction = 'withdraw';
      }, 500);

      this.displaySpaceStation = false;
      this.displayRocket = true;
      this.displayCounter = true;
      let nbBet = parseInt(this.nbBetElement.nativeElement.value);
      this.nbChips -= nbBet;

      if (this.user) {
        this.userApi.updateUserChips({ chips: this.nbChips }, this.user._id).subscribe();
      }

      let chance = this.randomInt(0, 1200);
      this.maxCounter = 0;
      if (chance >= 0 && chance < 190) {
        this.maxCounter = this.randomFloat(1, 3.99);
      } else if (chance > 190 && chance <= 250) {
        this.maxCounter = this.randomFloat(1, 11.99);
      } else if (chance > 250 && chance <= 400) {
        this.maxCounter = this.randomFloat(1, 5.99);
      } else if (chance > 400 && chance <= 430) {
        this.maxCounter = this.randomFloat(1, 21.99);
      } else if (chance > 430 && chance <= 460) {
        this.maxCounter = this.randomFloat(1, 1.99);
      } else if (chance > 460 && chance <= 700) {
        this.maxCounter = this.randomFloat(1, 1.99);
      } else if (chance > 700 && chance <= 850) {
        this.maxCounter = this.randomFloat(1, 1.03);
      } else if (chance > 850 && chance <= 890) {
        this.maxCounter = this.randomFloat(1, 36.03);
      } else if (chance > 890 && chance <= 980) {
        this.maxCounter = this.randomFloat(1, 6.03);
      } else if (chance > 980 && chance <= 1000) {
        this.maxCounter = this.randomFloat(1, 120.99);
      } else if (chance > 1000 && chance <= 1200) {
        this.maxCounter = this.randomFloat(1, 7.99);
      }
      this.counter = 1.0;
      this.duration = 60;
      this.countDown();
    }
  }

  countDown(): void {
    this.counter = parseFloat((this.counter + 0.01).toFixed(2));

    if (this.maxCounter > this.counter) {
      setTimeout(() => this.countDown(), this.duration);
    } else {
      this.visibilityButtonAction = false;

      this.historicalList.push(this.counter);

      this.displayExplosion = true;
      this.displayRocket = false;

      this.nbBetElement.nativeElement.value = 0;
      this.blockInput = false;

      setTimeout(() => {
        this.scrollBottomHistorical();
        this.displayExplosion = false;
        this.displayAstronaut = true;

        setTimeout(() => {
          this.displayAstronaut = false;
          this.displaySpaceStation = true;
          this.displayCounter = false;

          this.textButtonAction = 'Miser';
          this.buttonAction = 'bet';
          this.visibilityButtonAction = true;

          this.blockAllInButton = false;
        }, 1500);
      }, 500);
    }

    if (this.counter >= 3.0 && this.counter < 4.99) {
      this.duration = 50;
    } else if (this.counter >= 5.0 && this.counter < 9.99) {
      this.duration = 40;
    } else if (this.counter >= 10.0 && this.counter < 24.99) {
      this.duration = 25;
    } else if (this.counter >= 25.0 && this.counter < 49.99) {
      this.duration = 15;
    } else if (this.counter >= 50.0 && this.counter < 74.99) {
      this.duration = 8;
    } else if (this.counter >= 75.0) {
      this.duration = 5;
    }
  }

  withdraw(): void {
    this.textButtonAction = '>>';
    this.buttonAction = 'skip';

    this.nbChips += Math.floor(
      this.counter * this.nbBetElement.nativeElement.value
    );

    this.displayAddChips = true;
    this.addChips = Math.floor(
      this.counter * this.nbBetElement.nativeElement.value
    );

    if (this.user) {
      this.userApi.updateUserChips({ chips: this.nbChips }, this.user._id).subscribe();
    }

    setTimeout(() => {
      this.displayAddChips = false;
    }, 3000);
  }

  skip(): void {
    this.counter = this.maxCounter;
    this.blockInput = false;
  }

  scrollBottomHistorical(): void {
    this.historicalElement.nativeElement.scrollTop =
      this.historicalElement.nativeElement.scrollHeight;
  }
}
