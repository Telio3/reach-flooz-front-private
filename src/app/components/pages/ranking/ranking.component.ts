import { Component } from '@angular/core';
import { User } from 'src/app/core/models/user.model';
import { UserApiService } from 'src/app/core/services/api/user-api.service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss'],
})
export class RankingComponent {
  public isLoading: boolean = false;
  public usersList: User[] = [];

  constructor(private userApi: UserApiService) {
    this.userApi.ranked().subscribe((data) => {
      this.usersList = data;
      this.isLoading = true;
    });
  }
}
