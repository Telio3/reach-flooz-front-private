import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/core/models/user.model';
import { AuthApiService } from 'src/app/core/services/api/auth-api.service';
import { UserApiService } from 'src/app/core/services/api/user-api.service';
import { faPowerOff, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
export class AccountComponent implements OnInit {
  public isLoading: boolean = false;
  public user: User | null = null;
  public rightPanelActive: boolean = false;
  public inscriptionForm!: FormGroup;
  public errorInscriptionForm: string = '';
  public signinForm!: FormGroup;
  public errorSigninForm: string = '';

  public logoutIcon: IconDefinition = faPowerOff;

  constructor(
    private authApi: AuthApiService,
    private userApi: UserApiService,
    private authService: AuthService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.isLoading = false;

    this.authService.getUserById()?.subscribe(
      (data) => {
        this.user = data;
        this.isLoading = true;
      },
      (err) => {
        this.user = null;
        this.isLoading = true;
      }
    );

    this.initForm();
  }

  initForm(): void {
    this.inscriptionForm = this.fb.group({
      username: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
    });

    this.signinForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  inscription() {
    if (this.inscriptionForm.valid) {
      this.userApi.createUser(this.inscriptionForm.value).subscribe(
        (data) => {
          this.user = data;
        },
        (err) => {
          this.errorInscriptionForm = err.error.message;
        }
      );
    }
  }

  signin() {
    if (this.signinForm.valid) {
      this.authApi.login(this.signinForm.value).subscribe(
        (data) => {
          this.user = data;
        },
        (err) => {
          this.errorSigninForm = err.error.message;
        }
      );
    }
  }

  logout() {
    this.authApi.logout().subscribe();
    this.user = null;
  }
}
