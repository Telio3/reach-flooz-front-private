import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tokenFormat'
})
export class TokenFormatPipe implements PipeTransform {
  transform(bet: number): string {
    if (bet >= 1000 && bet < 1000000) {
      return Math.round((bet / 1000) * 10) / 10 + 'K';
    } else if (bet >= 1000000 && bet < 1000000000) {
      return Math.round((bet / 1000000) * 10) / 10 + 'M';
    } else if (bet >= 1000000000) {
      return Math.round((bet / 1000000000) * 10) / 10 + 'Md';
    } else {
      return bet.toString();
    }
  }
}
