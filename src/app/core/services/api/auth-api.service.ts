import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class AuthApiService {
  constructor(private http: HttpClient) {}

  login(body: object): Observable<User> {
    environment.httpOptions.params = new HttpParams();

    return this.http
      .post<User>(
        environment.url_api + '/auth/signin',
        body,
        environment.httpOptions
      )
      .pipe(
        map((res: User) => {
          return res;
        })
      );
  }

  logout() {
    environment.httpOptions.params = new HttpParams();

    return this.http
      .get(environment.url_api + '/auth/signout', environment.httpOptions)
      .pipe(
        map((res) => {
          return res;
        })
      );
  }
}
