import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserApiService {
  constructor(private http: HttpClient) {}

  createUser(body: object): Observable<User> {
    environment.httpOptions.params = new HttpParams();

    return this.http
      .post<User>(environment.url_api + '/users', body, environment.httpOptions)
      .pipe(
        map((res: User) => {
          return res;
        })
      );
  }

  updateUserChips(body: object, id: string): Observable<User> {
    environment.httpOptions.params = new HttpParams();

    return this.http
      .put<User>(
        environment.url_api + '/users/' + id,
        body,
        environment.httpOptions
      )
      .pipe(
        map((res: User) => {
          return res;
        })
      );
  }

  ranked(): Observable<User[]> {
    environment.httpOptions.params = new HttpParams();
    environment.httpOptions.params = environment.httpOptions.params.append(
      'ranked',
      1
    );

    return this.http
      .get<User[]>(environment.url_api + '/users', environment.httpOptions)
      .pipe(
        map((res: User[]) => {
          return res;
        })
      );
  }
}
