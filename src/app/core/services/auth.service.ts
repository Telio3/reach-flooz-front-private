import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private cookieService: CookieService, private http: HttpClient) {}

  getUserById(): Observable<User> {
    const jwt: string = this.cookieService.get('jwt');
    const idUser: string = this.cookieService.get('idUser');

    environment.httpOptions.params = new HttpParams();

    return this.http
      .get<User>(
        environment.url_api + '/users/' + (jwt && idUser ? idUser : 'null'),
        environment.httpOptions
      )
      .pipe(
        map((res: User) => {
          return res;
        })
      );
  }
}
