export class Card {
    constructor(
        public number: number,
        public value: string,
        public path: string,
        public suit: string,
        public color: string,
        public index: number,
        public isReturned: boolean
    ) {}
  }
  