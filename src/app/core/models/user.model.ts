export class User {
  constructor(
    public _id: string,
    public local: {
      email: string,
      password: string
    },
    public username: string,
    public chips: number
  ) {}
}
