import { Routes } from '@angular/router';
import { AccountComponent } from './components/pages/account/account.component';
import { BlackjackComponent } from './components/pages/blackjack/blackjack.component';
import { HomeComponent } from './components/pages/home/home.component';
import { RankingComponent } from './components/pages/ranking/ranking.component';
import { RouletteComponent } from './components/pages/roulette/roulette.component';
import { TakeOffComponent } from './components/pages/take-off/take-off.component';

export const ROUTE: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'take-off',
    component: TakeOffComponent,
  },
  // {
  //   path: 'roulette',
  //   component: RouletteComponent,
  // },
  {
    path: 'blackjack',
    component: BlackjackComponent,
  },
  {
    path: 'classement',
    component: RankingComponent,
  },
  {
    path: 'compte',
    component: AccountComponent,
  },
];
