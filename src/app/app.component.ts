import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements AfterViewInit {
  public mouseX: number = 0;
  public mouseY: number = 0;
  public cursorX: number = 0;
  public cursorY: number = 0;

  @ViewChild('cursor')
  public cursor!: ElementRef;

  constructor() {}

  ngAfterViewInit(): void {
    window.requestAnimationFrame(() => this.raf());
  }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(e: MouseEvent) {
    this.mouseX = e.clientX;
    this.mouseY = e.clientY;
  }

  lerp(start: number, end: number, amt: number): number {
    return (1 - amt) * start + amt * end;
  }

  raf(): void {
    this.cursorX = this.lerp(this.cursorX, this.mouseX, 0.1);
    this.cursorY = this.lerp(this.cursorY, this.mouseY, 0.1);

    this.cursor.nativeElement.style.transform =
      'translate3D(calc(-50% + ' +
      this.cursorX +
      'px), calc(-50% + ' +
      this.cursorY +
      'px), 0)';

    window.requestAnimationFrame(() => this.raf());
  }
}
